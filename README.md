# SmallImprovements.Blazor #

Project created to demonstrate how to build a simple application using Asp.Net Blazor WebAssembly.

The project was separated in 5 days:

- Day 1. Shows how to navigate between pages.
- Day 2. Shows how to authenticate a user and store the state in LocalStorage.
- Day 3. Shows how to interact with components.
- Day 4. Shows the usage of modals andChildContent.
- Day 5. Shows how to use CQRS and events to control the application.

The presentation slides can be found in [here](https://docs.google.com/presentation/d/1biT7tnNPXitqxYW7O8Xde7HMcDq9LerfHblar0rYtdA/edit?usp=sharing).