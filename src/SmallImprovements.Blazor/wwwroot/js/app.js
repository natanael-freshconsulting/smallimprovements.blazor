doLogin = () => {
    console.log('hi');
}

changeObjectiveBackground = async (colorsFunction) => {
    const value = DotNet.invokeMethod('SmallImprovements.Blazor', colorsFunction);
    const progressColors = JSON.parse(value);

    document.querySelectorAll('.activity-progress select').forEach(select => {
        select.style.background = progressColors[select.value];
    });
}