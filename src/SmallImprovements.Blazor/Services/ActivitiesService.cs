﻿using SmallImprovements.General.Dtos;
using SmallImprovements.General.Http;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SmallImprovements.Blazor.Services
{
    public class ActivitiesService : IActivitiesService
    {
        private readonly UserState _userState;
        private readonly ActivityState _activityState;

        public ActivitiesService(UserState userState, ActivityState activityState)
        {
            _userState = userState;
            _activityState = activityState;
        }

        public async Task<List<ActivityDto>> GetActivities()
        {
            using var client = new FakeHttpClient();
            var response = await client.GetAsync("activity");
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ActivityDto>>(json);
        }

        public async Task<List<ActivityDto>> GetObjectives()
        {
            using var client = new FakeHttpClient();
            var response = await client.GetAsync("activity?activityType=Objective");
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ActivityDto>>(json);
        }

        public async Task<List<ActivityDto>> GetPraises()
        {
            using var client = new FakeHttpClient();
            var response = await client.GetAsync("activity?activityType=Praise");
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ActivityDto>>(json);
        }

        public async Task CreatePraise(PraiseDto praise)
        {
            var url = "activity";
            praise.From = await _userState.GetUser();

            using var client = new FakeHttpClient();
            var response = await client.PostAsync(
                url,
                Serialize(praise));
            await _activityState.ActivityCreated();
        }

        public async Task CreateObjective(ObjectiveDto objective)
        {
            var url = "activity";
            objective.Owner = await _userState.GetUser();
            objective.Progress = ObjectiveProgress.Open;

            using var client = new FakeHttpClient();
            var response = await client.PostAsync(
                url,
                Serialize(objective));
            await _activityState.ActivityCreated();
        }

        public async Task UpdateObjectiveProgress(ObjectiveDto objective, ObjectiveProgress progress)
        {
            var url = $"activity/{objective.Id}";
            objective.Progress = progress;

            using var client = new FakeHttpClient();
            await client.PutAsync(
                url,
                Serialize(objective));
        }

        private StringContent Serialize(object dto)
        {
            return new StringContent(JsonConvert.SerializeObject(dto), Encoding.UTF8);
        }
    }
}