﻿using Blazored.LocalStorage;
using SmallImprovements.General.Dtos;
using System;
using System.Threading.Tasks;

namespace SmallImprovements.Blazor.Services
{
    public class UserState
    {
        private readonly ILocalStorageService _localStorage;

        public UserState(ILocalStorageService localStorage)
        {
            _localStorage = localStorage;
        }

        public event Func<Task> OnUserStateChanged;

        public async Task<bool> IsAuthenticated()
        {
            var user = await GetUser();
            return user != null;
        }

        public async Task<PersonDto> GetUser()
        {
            var containsPerson = await _localStorage.ContainKeyAsync("person");

            if (containsPerson)
            {
                return await _localStorage.GetItemAsync<PersonDto>("person");
            }

            return null;
        }

        public async Task SetUser(PersonDto person)
        {
            if (person == null)
            {
                await _localStorage.RemoveItemAsync("person");
            }
            else
            {
                await _localStorage.SetItemAsync("person", person);
            }
            await OnUserStateChanged();
        }
    }
}