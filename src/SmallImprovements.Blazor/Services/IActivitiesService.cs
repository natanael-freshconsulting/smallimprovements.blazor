﻿using SmallImprovements.General.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmallImprovements.Blazor.Services
{
    public interface IActivitiesService
    {
        Task<List<ActivityDto>> GetActivities();

        Task<List<ActivityDto>> GetObjectives();

        Task<List<ActivityDto>> GetPraises();

        Task CreatePraise(PraiseDto praise);

        Task CreateObjective(ObjectiveDto objective);
        Task UpdateObjectiveProgress(ObjectiveDto objective, ObjectiveProgress argsValue);
    }
}
