﻿using SmallImprovements.General.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmallImprovements.Blazor.Services
{
    public interface IUsersService
    {
        Task<List<PersonDto>> GetAll();
        Task Login(string email, string password);
        Task Logout();
    }
}
