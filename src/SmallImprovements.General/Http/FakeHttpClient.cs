using Microsoft.AspNetCore.WebUtilities;
using SmallImprovements.General.Data;
using SmallImprovements.General.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SmallImprovements.General.Http
{
    public class FakeHttpClient : IDisposable
    {
        private static SmallImprovementsRepository _repository = new SmallImprovementsRepository();

        public Task<FakeHttpResponse> GetAsync(string url)
        {
            var response = new FakeHttpResponse();
            switch (url)
            {
                case string personUrl when personUrl.Contains("person"):
                    response = GetPeople(personUrl);
                    break;
                case string activityUrl when activityUrl.Contains("activity"):
                    response = GetActivity(activityUrl);
                    break;
            }

            return Task.FromResult(response);
        }

        public async Task<FakeHttpResponse> PostAsync(string url, StringContent body)
        {
            var dto = await Deserialize(body);
            var response = new FakeHttpResponse();

            switch (url)
            {
                case string activityUrl when activityUrl.Contains("activity"):
                    response = CreateActivity(dto);
                    break;
                case string authUrl when authUrl.Contains("auth"):
                    response = Authenticate(dto);
                    break;
            }

            return response;
        }

        public async Task<FakeHttpResponse> PutAsync(string url, StringContent body)
        {
            var dto = await Deserialize(body);
            var id = Guid.Parse(url.Replace("activity/", ""));
            var objective = _repository.Activities.FirstOrDefault(a => a.Id == id);
            ((ObjectiveDto)objective).Progress = ((ObjectiveDto)dto).Progress;
            return new FakeHttpResponse(JsonConvert.SerializeObject(objective));
        }

        public Task DeleteAsync(string url)
        {
            throw new System.NotImplementedException();
        }

        public void Dispose()
        {
        }

        private async Task<object> Deserialize(StringContent body)
        {
            var json = await body.ReadAsStringAsync();
            var jsonLower = json.ToLower();

            if (jsonLower.Contains("progress") && jsonLower.Contains("owner"))
            {
                return JsonConvert.DeserializeObject<ObjectiveDto>(json);
            }

            if (jsonLower.Contains("from") && jsonLower.Contains("to"))
            {
                return JsonConvert.DeserializeObject<PraiseDto>(json);
            }

            if (jsonLower.Contains("email") && jsonLower.Contains("name"))
            {
                return JsonConvert.DeserializeObject<PersonDto>(json);
            }

            if (jsonLower.Contains("email") && jsonLower.Contains("password"))
            {
                return JsonConvert.DeserializeObject<AuthDto>(json);
            }

            if (jsonLower.Contains("name"))
            {
                return JsonConvert.DeserializeObject<TeamDto>(json);
            }

            return null;
        }

        private FakeHttpResponse GetPeople(string url)
        {
            url = url.Replace("person", "");
            var queryParams = QueryHelpers.ParseQuery(url);

            var peopleQuery = _repository.People.AsQueryable();
            var json = string.Empty;

            if (queryParams.TryGetValue("email", out var email))
            {
                peopleQuery = peopleQuery.Where(p => p.Email == email);
                json = JsonConvert.SerializeObject(peopleQuery.FirstOrDefault());
            }
            else
            {
                json = JsonConvert.SerializeObject(peopleQuery.ToList());
            }

            return new FakeHttpResponse(json);
        }

        private FakeHttpResponse GetActivity(string url)
        {
            url = url.Replace("activity?", "");
            var queryParams = QueryHelpers.ParseQuery(url);

            var query = _repository.Activities
                .OrderByDescending(a => a.Creation)
                .AsQueryable();

            if (queryParams.TryGetValue("activityType", out var activityTypeString))
            {
                if (Enum.TryParse<ActivityType>(activityTypeString, out var activityType))
                {
                    query = query.Where(e => e.ActivityType == activityType);
                }
            }

            return new FakeHttpResponse(JsonConvert.SerializeObject(query.Take(15).ToList()));
        }

        private FakeHttpResponse CreateActivity(object dto)
        {
            switch (dto)
            {
                case PraiseDto praiseDto:
                    praiseDto.To = _repository.People.FirstOrDefault(p => p.Id == praiseDto?.To?.Id);
                    _repository.Activities.Add(praiseDto);
                    break;
                case ObjectiveDto objectiveDto:
                    _repository.Activities.Add(objectiveDto);
                    break;
            }

            return new FakeHttpResponse(JsonConvert.SerializeObject(dto));
        }

        private FakeHttpResponse Authenticate(object dto)
        {
            if (!(dto is AuthDto authDto))
            {
                return new FakeHttpResponse();
            }


            var authUser = _repository.People.FirstOrDefault(p =>
                p.Email == authDto.Email &&
                p.Password == authDto.Password);

            return authUser == null ? new FakeHttpResponse() : new FakeHttpResponse("jwt-token");
        }
    }
}