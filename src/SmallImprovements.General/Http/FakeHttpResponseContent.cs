using System.Threading.Tasks;

namespace SmallImprovements.General.Http
{
    public class FakeHttpResponseContent
    {
        private readonly string _json;

        public FakeHttpResponseContent(string json)
        {
            _json = json;
        }

        public Task<string> ReadAsStringAsync()
        {
            return Task.FromResult(_json);
        }
    }
}