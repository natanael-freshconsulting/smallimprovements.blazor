using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace SmallImprovements.General.Dtos
{
    public enum ActivityType
    {
        Praise, Objective
    }

    [JsonConverter(typeof(ActivityDtoConverter))]
    public abstract class ActivityDto : BaseDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public abstract ActivityType ActivityType { get; }
    }

    public class BaseSpecifiedConcreteClassConverter : DefaultContractResolver
    {
        protected override JsonConverter ResolveContractConverter(Type objectType)
        {
            if (typeof(ActivityDto).IsAssignableFrom(objectType) && !objectType.IsAbstract)
                return null;
            return base.ResolveContractConverter(objectType);
        }
    }

    public class ActivityDtoConverter : JsonConverter
    {
        static JsonSerializerSettings SpecifiedSubclassConversion = new JsonSerializerSettings() { ContractResolver = new BaseSpecifiedConcreteClassConverter() };

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(ActivityDto));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jo = JObject.Load(reader);
            switch (jo[nameof(ActivityDto.ActivityType)].Value<int>())
            {
                case (int)ActivityType.Objective:
                    return JsonConvert.DeserializeObject<ObjectiveDto>(jo.ToString(), SpecifiedSubclassConversion);
                case (int)ActivityType.Praise:
                    return JsonConvert.DeserializeObject<PraiseDto>(jo.ToString(), SpecifiedSubclassConversion);
            }

            throw new NotImplementedException();
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException(); // won't be called because CanWrite returns false
        }
    }
}